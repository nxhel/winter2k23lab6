public class Board
{
 private Die Die1;
 private Die Die2;
 private Boolean[] tiles;
 
 public Board()
 {
   //assigning the fields
   this.Die1= new Die();
   this.Die2= new Die();
   this.tiles= new Boolean[]{false, false, false, false, false, false, false, false, false, false , false, false};
   
   //ALTERNATIVE
   //Boolean[] tiles = new Boolean[12];
   //Arrays.fill(tiles, Boolean.FALSE);
 }
   
  public String arrayPosition()
  {
    String position=" ";
     
    for (int i =0; i<this.tiles.length;i++)
     {
       if(this.tiles[i]==false)
       {
         position+=(i+1)+" ";
       }
       else
       {
         position+=" X ";
       }
    }
      return position; 
     }
  
  
  public Boolean playATurn()
  {
    //calling method to get a random value
    this.Die1.roll();
    this.Die2.roll();
    
    //print the objects
    System.out.println("Value of Die 1 " + Die1.toString());
    System.out.println("Value of Die 2 " + Die2.toString());
    
    int sumOfDice= Die1.getFaceValue()+ Die2.getFaceValue();
    
    if(this.tiles[sumOfDice-1]==false)
    {
      this.tiles[sumOfDice-1]=true;
      System.out.println("Closing tile equal to sum of: " + sumOfDice);
      return false;
    }
    else if(this.tiles[Die1.getFaceValue()]==false)
    {
      this.tiles[Die1.getFaceValue()]=true;
      System.out.println("Closing tile with the same value as die one: " + Die1.getFaceValue());
      return false;
    }
    else if(this.tiles[Die2.getFaceValue()]==false)
    {
      this.tiles[Die2.getFaceValue()]=true;
      System.out.println("Closing tile with the same value as die two: " + Die2.getFaceValue());
      return false;
    }
    else
    {
     System.out.println("All the tiles for these values are already shut ");
     return true;
    }
    
  }
    
   public String toString()
   {
    return arrayPosition();
   }
   
 }


       
   
   
   
   
   
   
 