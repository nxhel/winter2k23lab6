import java.util.Random;

public class Die
{
 private int faceValue;
 private Random numGen; 
//we are giving a name to the Object named Random

public Die()
{
 this.faceValue=1;
 this.numGen= new Random();
//Initialising the Object Random
}

public void roll()
{
 this.faceValue=this.numGen.nextInt(6)+1;
}

public int getFaceValue()
{
 return this.faceValue;
}

public String toString()
{
 return Integer.toString(faceValue);
}
}

   